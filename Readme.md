# Note:

 None of the Master or Worker nodes should have swap memory enabled.

## Get latest webui dashboard:

	https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/


### Quorum explaination in stacked HA architecture:

  The min. requirement for HA cluster is 3 nodes, but it is still allowed to form cluster with 2 nodes.
  The moment any one node goes down. The writing to ETCD stops, and with it the api-server too stops 
  functioning as it cannot communicate with etcd server. Kubelet re-spawns the api-server and etcd-server 
  containers but they keep shutting down as etcd cluster quorum is not satisfied.

  Reason:
  
  Suppose we have a 2 node cluster w/ nodes A and B. 

  When node B goes down, etcd of A can no longer communicate with that of B.  
  It disallows any new writes and as a result, etcd-server and api-server on A 
  keeps shutting down after a kubelet restart the containers after a wait period.
  This is a safety measure. 
  In order for etcd to consider a successful write operation, the new data should be 
  updated on all etcd nodes. 





## Istio Disable 3rd party jwt tokens during installatiion prompt

	https://istio.io/latest/docs/ops/best-practices/security/#configure-third-party-service-account-tokens


